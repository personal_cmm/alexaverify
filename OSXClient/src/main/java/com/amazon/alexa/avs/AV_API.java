package com.amazon.alexa.avs;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.File;
import java.io.IOException;

import static com.amazon.alexa.avs.AV_Audio.fileName;


/**
 * Created by claudius on 4/16/16.
 */
public class AV_API {

    public static Oxford.VerificationResult veriResult = null;
    private static RequestBody requestFile = RequestBody.create(MediaType.parse("application/octet-stream"), fileName);
    public static Boolean isDoneVerifying = false;
//    public static final String defaultPofile = "08ebc30b-f997-4e3a-b3fb-3065da8a7e51";//"a2b8f668-c450-49c7-a8ee-e1ee9b4b8645";

    private static Oxford api = buildRequest().create(Oxford.class);
    static String vid = (String) AV_Profiler.profiles.get(AV_Profiler.profiles.size() - 1);

    private static Retrofit buildRequest() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Oxford.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }

    public static Boolean createSpeaker() {
        RequestBody local = RequestBody.create(MediaType.parse("application/json"), "{\"locale\":\"en-us\"}");
        try {
            Response response = api.create(local).execute();

            if (response.code() == 200) {
                Oxford.CreateResult res = (Oxford.CreateResult) response.body();
                vid = res.verificationProfileId;

                AV_Profiler.write(vid);

                System.out.println("------------- CREATE 2 ------------");
                System.out.println("vID: " + vid);
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static Boolean enrollSpeaker(File file) throws IOException {

        System.out.println("vid: " + vid);

        Response response = api.enroll(vid,
                RequestBody.create(MediaType.parse("application/octet-stream"),
                        file)).execute();

        System.out.println("enroll: " + response.raw());

        if (response.code() == 200) {
            Oxford.EnrollResult res = (Oxford.EnrollResult) response.body();

            System.out.println("------------- ENROLLMENT 2 ------------");
            System.out.println("Count: " + res.enrollmentsCount);
            System.out.println("Status: " + res.enrollmentStatus);
            System.out.println("Remaining: " + res.remainingEnrollments);
            System.out.println("Phrase: " + res.phrase);

            return true;
        }

        return false;
    }


    public static void verify(String profile) {
        if (!fileName.exists()) {
            return;
        }

        try {
//            RequestBody body = RequestBody.create(MediaType.parse("application/octet-stream"), new File("/Users/claudius/Documents/vlc/pass-test11.wav"));
            Response response = api.verify(profile, requestFile).execute();

//        Response response = api.verify(vid, requestFile).execute();

            if (response.code() > 1) {
                isDoneVerifying = true;
            }

            veriResult = (Oxford.VerificationResult) response.body();

            if (response.code() == 200) {
                System.out.println("------------- VERIFICATION 2 ------------");
                System.out.println("Result: " + veriResult.result);
                System.out.println("Confidence: " + veriResult.confidence);
                System.out.println("Phrase: " + veriResult.phrase);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return;
    }
}


