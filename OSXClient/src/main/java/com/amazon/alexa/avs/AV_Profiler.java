package com.amazon.alexa.avs;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by claudius on 4/16/16.
 */
public class AV_Profiler {

    public static ArrayList<String> profiles = new ArrayList<>();
    static String sFile = "/Users/claudius/Documents/vlc/Profiles.txt";
    static File fFile = new File(sFile);

    public static void read() {
        if (!fFile.exists()) {
            try {
                fFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        profiles.clear();

        try {
            FileReader reader = new FileReader(sFile);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if (!line.equals("\n")) {
                    profiles.add(line);
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void write(String profile) {
        try {
            FileWriter writer = new FileWriter(sFile, true);
            writer.write(profile);
            writer.write("\n");   // write new line
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
