package com.amazon.alexa.avs;

import java.io.File;
import java.io.IOException;

/**
 * Created by claudius on 4/16/16.
 */
public class AV_Verify {

    public static void createProfile() {
        int userNum = AV_Profiler.profiles.size();
        AV_Audio.run("/Users/claudius/Documents/vlc/user" + (userNum + 1) + ".wav");
        while (!AV_Audio.recordDone) {
            //do nothing
        }
        AV_Audio.recordDone = false;

        Boolean created = AV_API.createSpeaker();
//        while (!created) {
//            created = AV_API.createSpeaker();
//        }
    }

    public static Boolean makeEnrollment(int count) {
        AV_Audio.run("/Users/claudius/Documents/vlc/enroll_" + count + ".wav");
        while (!AV_Audio.recordDone) {
            //do nothing
        }
        AV_Audio.recordDone = false;
        Boolean result = false;
        try {
            File file = new File("/Users/claudius/Documents/vlc/enroll_" + count + ".wav");
            if (!file.exists()) {
                return false;
            }

            result = AV_API.enrollSpeaker(new File("/Users/claudius/Documents/vlc/enroll_" + count + ".wav"));
//            while (!result) {
//                result = AV_API.enrollSpeaker(new File("/Users/claudius/Documents/vlc/enroll_" + count + ".wav"));
//            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void makeVerification(String profile, Boolean withAudio) {
        if(withAudio) {
            AV_Audio.run("");
            while (!AV_Audio.recordDone) {
                //do nothing
            }
            AV_Audio.recordDone = false;
        }
        AV_API.verify(profile);
    }
}

