package com.amazon.alexa.avs;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface Oxford {

    String BASE_URL = "https://api.projectoxford.ai/spid/v1.0/";
    String id = "08ebc30b-f997-4e3a-b3fb-3065da8a7e51";

    @Headers({
            "Content-Type: application/json",
            "Ocp-Apim-Subscription-Key: f478865713474c819192f0f18159a754"
    })
    @POST("verificationProfiles")
    Call<CreateResult> create(@Body RequestBody local);

    @Headers({
            "Content-Type: application/octet-stream",
            "Ocp-Apim-Subscription-Key: f478865713474c819192f0f18159a754"
    })
    @POST("verificationProfiles/{verificationProfileId}/enroll")
    Call<EnrollResult> enroll(@Path("verificationProfileId") String verificationProfileId, @Body RequestBody audioFile);

    @Headers({
            "Content-Type: application/octet-stream",
            "Ocp-Apim-Subscription-Key: f478865713474c819192f0f18159a754"
    })
    @POST("verify")
    Call<VerificationResult> verify(@Query("verificationProfileId") String verificationProfileId, @Body RequestBody audioFile);

    class VerificationResult {
        public String result;
        public String confidence;
        public String phrase;
    }

    class EnrollResult {
        public String enrollmentStatus;
        public String enrollmentsCount;
        public String remainingEnrollments;
        public String phrase;
    }

    class CreateResult {
        public String verificationProfileId;
    }

}
