package com.amazon.alexa.avs;

import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

/**
 * Created by claudius on 4/16/16.
 */
public class AV_Audio implements LineListener {

    public static final File fileName = new File("/Users/claudius/Documents/vlc/verify.wav");
    public static final int TIMER = 3000;
    public static Boolean recordDone = false;
    public static Boolean playCompleted = false;


    private static TargetDataLine dataLine;
    private static AudioInputStream audioInputStream;

    private static void doRecord(File fileName) {
        try {
            //get format
            AudioFormat format = AudioInputFormat.LPCM.getAudioFormat();
            //create new DataLine
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            dataLine = (TargetDataLine) AudioSystem.getLine(info);
            audioInputStream = new AudioInputStream(dataLine);
            dataLine.open(format);
            dataLine.start();
            //write to file
            AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, fileName);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (LineUnavailableException e) {
            e.printStackTrace();
        }
    }

    public static void record() {
        doRecord(fileName);
    }

    public static void record(File fileName) {
        doRecord(fileName);
    }


    public static void stop() {
        if (audioInputStream != null) {
            try {
                audioInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (dataLine != null) {
            dataLine.stop();
            dataLine.close();
        }
        recordDone = true;
    }

    public static void run(String fileName) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                if (!fileName.isEmpty()) {
                    AV_Audio.record(new File(fileName));
                } else {
                    AV_Audio.record();
                }
            }
        };

        thread.start();
        try {
            thread.join(TIMER);
            if (thread.isAlive()) {
                while (!thread.isInterrupted()) {
                    thread.interrupt();
                }
            }
            AV_Audio.stop();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//    public static void play(int num) {
//        try {
//            File user =  new File("/Users/claudius/Documents/vlc/user1.wav");
//            AudioInputStream stream;
//            AudioFormat format;
//            DataLine.Info info;
//            Clip clip;
//
//            stream = AudioSystem.getAudioInputStream(user);
//            format = stream.getFormat();
//            info = new DataLine.Info(Clip.class, format);
//            clip = (Clip) AudioSystem.getLine(info);
//            clip.open(stream);
//            clip.start();
//        }
//        catch (Exception e) {
//            //whatevers
//        }
//    }

    public void play(String audio) {

        playCompleted = false;

//        for (String file : audio) {
        File audioFile = new File(audio);

        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

            AudioFormat format = audioStream.getFormat();

            DataLine.Info info = new DataLine.Info(Clip.class, format);

            Clip audioClip = (Clip) AudioSystem.getLine(info);

            audioClip.addLineListener(this);

            audioClip.open(audioStream);

            audioClip.start();

            while (!playCompleted) {
                // wait for the playback completes
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            audioClip.close();

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }
    }

//    }


    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
//            System.out.println("Playback started.");
        } else if (type == LineEvent.Type.STOP) {
            playCompleted = true;
//            System.out.println("Playback completed.");
        }
    }
}
